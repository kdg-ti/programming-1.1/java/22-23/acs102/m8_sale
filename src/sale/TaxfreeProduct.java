package sale;

public class TaxfreeProduct extends Product{

  public TaxfreeProduct(int number, String description, double price) {
    super(number, description, price, 0.0);
  }

  @Override
  public String toString() {
    return String.format("%d %s %c%.2f",getNumber(),getDescription(),'\u20AC',getPtrice());
  }

  public String[] getAllergenes() {
    return new String[0];
  }
}
