package sale;


public class Sale implements VatTransaction{
  private int numberOfSalesLines=0;
  private SalesLine[]  lines= new SalesLine[100] ;


  @Override
  public double getImportTax() {
    return 0;
  }

  @Override
  public double getPtrice() {
    return 0;
  }

  @Override
  public double getPriceVatInclusive() {
    return 0;
  }

  public void add(SalesLine line){
    if (numberOfSalesLines < 100) {
      lines[numberOfSalesLines++] = line;
    } else{
      System.err.println("Cannot add more than 100 lines to a sale. Salesline refused: " + line);
    }
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();
    for (int i = 0; i < numberOfSalesLines; i++) {
      builder.append(lines[i])
          .append("\n");
    }
    return builder.toString();
  }
}
