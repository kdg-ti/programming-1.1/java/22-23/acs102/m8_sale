package sale;

public class SalesLine implements VatTransaction{
  int quantity;
  Product product;

  public SalesLine(Product product,int quantity ) {
    setQuantity( quantity) ;
    setProduct(product);
  }

  public void setProduct(Product product){
    this.product = product;
  }

  public void setQuantity(int quantity) {
    this.quantity = quantity;
  }

  @Override
  public double getImportTax() {
    return 0;
  }

  @Override
  public double getPtrice() {
    return 0;
  }

  @Override
  public double getPriceVatInclusive() {
    return 0;
  }

  @Override
  public String toString() {
    return String.format("%sx %s",quantity,product);
  }
}
