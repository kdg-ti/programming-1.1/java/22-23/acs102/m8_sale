package sale;

public interface VatTransaction {
  double getImportTax();
  double getPtrice();
  double getPriceVatInclusive();
}
