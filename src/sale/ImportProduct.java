package sale;

public class ImportProduct extends Product{
  double importTax;

  public ImportProduct(int number, String description, double price, double vatPercentage,double importTax) {
    super(number, description, price, vatPercentage);
    setImportTax(importTax);
  }

  @Override
  public double getImportTax() {
    return importTax;
  }

  public void setImportTax(double importTax) {
    this.importTax = importTax;
  }

  @Override
  public String toString() {
    return String.format( "%s \u20AC%.1f",super.toString(),getImportTax()) ;
  }
}
