package sale;

public  class Product implements VatTransaction {
  private double vatPercentage;
  private int number;
  private String description;
  private double price;

  public Product(int number, String description,  double price,double vatPercentage) {
    setVatPercentage( vatPercentage);
    setNumber( number);
    setDescription(description);
    setPrice( price);
  }

  public double getVatPercentage() {
    return vatPercentage;
  }

  public void setVatPercentage(double vatPercentage) {
    if (vatPercentage >= 0 && vatPercentage <= 100){
      this.vatPercentage = vatPercentage;
    } else{
      System.err.println("Vat percentage should be between 0 and 100. Got "+vatPercentage);
    }
  }

  public int getNumber() {
    return number;
  }

  public void setNumber(int number) {
    if(number>=0) {
      this.number = number;
    }  else{
      System.err.println("Number should be positive. Got "+number);
    }
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    if(description != null && description.length() > 0) {
      this.description = description;
    } else{
      System.err.println("Description should noty be empty ");
    }
  }

  public double getPrice() {
    return price;
  }

  public void setPrice(double price) {
    if(price>=0) {
      this.price = price;
    }  else{
      System.err.println("Number should be positive. Got "+price);
    }
  }

  @Override
  public double getImportTax() {
    return 0;
  }

  @Override
  public double getPtrice() {
    return price;
  }

  @Override
  public double getPriceVatInclusive() {
    return price * (100 + getVatPercentage())/100;
  }

  @Override
  public String toString() {
    return String.format("%d %s %c%.2f %.0f%%",getNumber(),getDescription(),'\u20AC',getPtrice(),getVatPercentage());
  }

 // public abstract String[] getAllergenes();
}
